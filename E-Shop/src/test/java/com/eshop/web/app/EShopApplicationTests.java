package com.eshop.web.app;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import com.eshop.web.app.entity.Category;
import com.eshop.web.app.entity.User;
import com.eshop.web.app.models.CategoryModel;
import com.eshop.web.app.models.UserRequestModel;
import com.eshop.web.app.service.CategoryService;
import com.eshop.web.app.servicesIMPL.OrderServiceIMPL;
import com.eshop.web.app.servicesIMPL.UserServiceImpl;

@SpringBootTest
public class EShopApplicationTests {
	
	@Autowired
	UserServiceImpl userServiceImpl;

	@Autowired
	OrderServiceIMPL orderServiceIMPL;
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	RestTemplate template;
	
	private static int  counter = 0;

	
	@BeforeEach
    void beforeEachTest() {
		counter++;
		System.out.println("Running before each test!!!");
    }

	@AfterEach
    void afterEachTest() {
		System.out.println("Running after each test!!!======> "+counter);
    }
	
	@Test
	void contextLoads() {

	}

	@Test
	void testSignup() {
		assertTrue(userServiceImpl.findAll().size() == 0);					

		UserRequestModel model = new UserRequestModel("avinash","singhavinash858@gmail.com","1234","9885810820");
		User user = userServiceImpl.createUser(model);
		
		assertNotNull(user);
		
		assertEquals("avinash", user.getName());
		assertEquals(model.getEmail(),user.getEmail());
		assertEquals(model.getPhone(),user.getPhone());
		assertSame(model.getName(), user.getName());	

		assertTrue(userServiceImpl.findAll().size() > 0);					
	}
	
	@Test
	void testCategory() {
//		ResponseEntity<Category> cat = template.postForEntity(baseUrl+"/api/categories", model, Category.class);
		CategoryModel model = new CategoryModel("Shoe");
		Category  cat = categoryService.createCategory(model);
		System.out.println("category name =====>"+cat);
	}

}
