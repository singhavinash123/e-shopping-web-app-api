package com.eshop.web.app.models;

import lombok.Data;

@Data
public class PasswordResetRequestModel {
	private String email;

}
