package com.eshop.web.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eshop.web.app.entity.Category;
import com.eshop.web.app.entity.User;

@Repository
public interface CategoryRepository extends  JpaRepository<Category, String>{
	Category findByName(String tittle);
}

