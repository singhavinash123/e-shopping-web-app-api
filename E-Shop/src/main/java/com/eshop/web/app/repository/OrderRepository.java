package com.eshop.web.app.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.eshop.web.app.entity.Order;

@Repository
public interface OrderRepository extends  JpaRepository<Order, String>{
	List<Order> findByUserId(String userId);

	@Query(value = "SELECT * FROM ORDERS o WHERE o.CREATED_DATE >= ?1 AND o.CREATED_DATE <= ?2" , nativeQuery = true)
	List<Order> findAllOrdersBetweenDate(Date before30DaysDate, Date todayDate);

	@Query(value = "SELECT SUM(PRICE*QUANTITY) FROM ORDERS O WHERE O.CREATED_DATE  >= ?1 AND O.CREATED_DATE <= ?2" , nativeQuery = true)
	String findTotalSalesBetweenDate(Date before30DaysDate, Date todayDate);
}


