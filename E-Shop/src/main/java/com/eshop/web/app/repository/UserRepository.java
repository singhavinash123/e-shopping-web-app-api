package com.eshop.web.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eshop.web.app.entity.User;

@Repository
public interface UserRepository extends  JpaRepository<User, String>{
	// select from USER_TBL where email = 'singhavinash857@gmail.com'
	User findByEmail(String email);
}

