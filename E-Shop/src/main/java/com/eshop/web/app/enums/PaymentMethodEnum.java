package com.eshop.web.app.enums;

public enum PaymentMethodEnum {
	PENDING("pending"),
	COMPLETED("completed");
	
	private String status;
	
	PaymentMethodEnum(String status){
		this.status = status;
	}
	
	public String status() {
		return status;
	}
	
}
