package com.eshop.web.app.service;

import java.util.List;

import com.eshop.web.app.entity.Category;
import com.eshop.web.app.models.CategoryModel;

public interface CategoryService {
	Category createCategory(CategoryModel model);
	Category findCategoryById(String category);
	List<Category> getAllCategories();
	Category deleteCategoryById(String categoryId);
}
