package com.eshop.web.app.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eshop.web.app.entity.Order;
import com.eshop.web.app.entity.Product;
import com.eshop.web.app.entity.User;
import com.eshop.web.app.models.AdminSummaryReport;
import com.eshop.web.app.models.AdminSummaryReportModel;
import com.eshop.web.app.models.ProductWise30DaysSummary;
import com.eshop.web.app.service.OrderService;
import com.eshop.web.app.service.ProductService;
import com.eshop.web.app.service.UserService;
import com.eshop.web.app.util.Utils;

@RestController
@RequestMapping("/api/summary")
public class AdminSummaryController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private OrderService orderService;
	
	@GetMapping()
	public ResponseEntity<?> getSummary(){
		
		logger.info("start method getSummary::");
		Date todayDate  = Utils.getTodayDate();
		Date before30DaysDate = Utils.substractDays(todayDate,30);
		String totalUserRegistered = userService.findAllUserDetailsBetweenDate(before30DaysDate,todayDate);
		String orders = orderService.findAllOrdersBetweenDate(before30DaysDate,todayDate);
		String salesAmount = orderService.findTotalSalesBetweenDate(before30DaysDate,todayDate);
		
		
		System.out.println();
		List<Order> listOrder = orderService.findAllOrdersDetailsBetweenDate(before30DaysDate, todayDate);
		
		AdminSummaryReportModel model = new AdminSummaryReportModel();
		
		for(Order order : listOrder) {
			model.getLast30DaysSummary().setUserRegistered(totalUserRegistered);
			model.getLast30DaysSummary().setSale(salesAmount);
			model.getLast30DaysSummary().setOrders(orders);
			
			
			Product productObj = productService.getProductByID(order.getProductId());
			
			ProductWise30DaysSummary  productWise30DaysSummary = new ProductWise30DaysSummary();
			
			productWise30DaysSummary.setProduct(productObj);
			productWise30DaysSummary.setQuantity(order.getQuantity());
			productWise30DaysSummary.setTotalSale(productObj.getPrice() * order.getQuantity());
			productWise30DaysSummary.setProductId(productObj.getId());
			
			model.getLast30DaysSummary().getProductWise30DaysSummary().add(productWise30DaysSummary);
			
		}
		List<Order> overAllOrders = orderService.findAll();
		List<User> overAllUsers = userService.findAll();
		List<Product> overAllProduct = productService.findAll();
		
		
		AdminSummaryReport report = new AdminSummaryReport();
		
		report.setResult(model);
		report.getResult().getOverAll().setOrders(overAllOrders.size());
		report.getResult().getOverAll().setProducts(overAllProduct.size());
		report.getResult().getOverAll().setUsers(overAllUsers.size());
		
		logger.info("totalUserRegistered {}",totalUserRegistered);
		logger.info("orders {}",orders);
		logger.info("salesAmount {}",salesAmount);

		logger.info("end method getSummary::");

		return new ResponseEntity<>(report,HttpStatus.OK);
	}
	
}
