package com.eshop.web.app.models;

import java.util.ArrayList;
import java.util.List;

import com.eshop.web.app.entity.Category;

import lombok.Data;

@Data
public class AllCategoriesResponse {
   List<Category> categories = new ArrayList<>();
}
