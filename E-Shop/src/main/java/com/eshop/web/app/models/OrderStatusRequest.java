package com.eshop.web.app.models;

import lombok.Data;

@Data
public class OrderStatusRequest {
	private String status;
}
