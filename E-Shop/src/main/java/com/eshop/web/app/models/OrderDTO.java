package com.eshop.web.app.models;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class OrderDTO {
		private String firstName;
		private String lastName;
		private String address;
		private List<ProductDTO> products = new ArrayList<>();
}
