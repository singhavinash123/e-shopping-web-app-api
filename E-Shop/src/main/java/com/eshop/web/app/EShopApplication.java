package com.eshop.web.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/*
 * developer name: 
 */
@SpringBootApplication
public class EShopApplication {

//	@Autowired
//	private UserRepository repository;
	
//	@PostConstruct
//	public void initUsers() {
//		List<User> users = Stream.of(
//				new User(101, "avinash", "password1", "singhavinash857@gmail.com"),
//				new User(102, "abhay", "password2", "user3@gmail.com"),
//				new User(103, "Madhu", "password3", "user4@gmail.com")
//				
//				).collect(Collectors.toList());
//		repository.saveAll(users);
//	}
	
	public static void main(String[] args) {
		SpringApplication.run(EShopApplication.class, args);
	}
	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}


}
