package com.eshop.web.app.models;

import java.util.Date;

import com.eshop.web.app.entity.Product;
import com.eshop.web.app.entity.User;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class OrderResponseModel {
	private int quantity;
	private String paymentMethod;
	private String status;

	@JsonProperty("_id")
	private String orderId;

	private String firstName;
	private String lastName;
	private String address;

	private Product product;
	private User user;
	
	private double price;
	
	@JsonProperty("created_at")
	private Date createdAt;
	private Date updatedAt;
}
