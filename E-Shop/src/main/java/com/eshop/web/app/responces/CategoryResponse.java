package com.eshop.web.app.responces;

import com.eshop.web.app.entity.Category;

import lombok.Data;

@Data
public class CategoryResponse {
	private String message;
	private Category category;
}
