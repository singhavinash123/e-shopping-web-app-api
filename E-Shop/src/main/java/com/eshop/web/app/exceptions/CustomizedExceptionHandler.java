package com.eshop.web.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.eshop.web.app.models.ErrorResponseModel;
import com.eshop.web.app.models.MessageModel;

@ControllerAdvice
@RestController
public class CustomizedExceptionHandler extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleAllExceptions(Exception ex){
		ErrorResponseModel errorModel = new ErrorResponseModel();
		errorModel.setError(new MessageModel(ex.getMessage()));
		errorModel.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		return new ResponseEntity<>(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(FileExceptions.class)
	public ResponseEntity<?> handleFileExceptions(FileExceptions ex){
		ErrorResponseModel errorModel = new ErrorResponseModel();
		errorModel.setError(new MessageModel(ex.getMessage()));
		errorModel.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		return new ResponseEntity<>(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<?> handleUserExceptions(UserNotFoundException ex){
		ErrorResponseModel errorModel = new ErrorResponseModel();
		errorModel.setError(new MessageModel(ex.getMessage()));
		errorModel.setStatus(HttpStatus.NOT_FOUND.value());
		return new ResponseEntity<>(errorModel, HttpStatus.NOT_FOUND);
	}
}
