package com.eshop.web.app.models;

import lombok.Data;

@Data
public class CatgeoryDeletedResponse {
	private String  message;
	private Result result = new Result();
}
