package com.eshop.web.app.service;

import java.util.List;

import com.eshop.web.app.entity.Product;
import com.eshop.web.app.models.GetAllProductsResponse;

public interface ProductService {
	Product createProduct(Product product);
	Product getProductByID(String productId);
	GetAllProductsResponse getAllProducts();
	GetAllProductsResponse getAllProductsByCategory(String categoryId);
	List<Product> findAll();
}
