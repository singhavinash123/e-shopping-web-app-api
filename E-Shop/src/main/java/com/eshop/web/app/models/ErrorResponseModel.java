package com.eshop.web.app.models;

import lombok.Data;

@Data
public class ErrorResponseModel {
	private MessageModel error;
	private int status;
}
	