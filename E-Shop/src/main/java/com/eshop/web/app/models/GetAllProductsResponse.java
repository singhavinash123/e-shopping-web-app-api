package com.eshop.web.app.models;

import java.util.ArrayList;
import java.util.List;

import com.eshop.web.app.entity.Product;

import lombok.Data;

@Data
public class GetAllProductsResponse {
	private int count;
	private List<Product> products = new ArrayList<>();
}
