package com.eshop.web.app.service;

import java.util.Date;
import java.util.List;

import com.eshop.web.app.entity.User;
import com.eshop.web.app.models.UserRequestModel;

public interface UserService {
	User createUser(UserRequestModel model);
    User loadUserByUsername(String string);
	User findByUserId(String userId);
	String findAllUserDetailsBetweenDate(Date before30DaysDate, Date todayDate);
	List<User> findAll();
}
