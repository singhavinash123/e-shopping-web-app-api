package com.eshop.web.app.models;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Last30DaysSummary {
	private String userRegistered;
	private String sale;
	private String orders;
	private List<ProductWise30DaysSummary> productWise30DaysSummary = new ArrayList<>();
}
