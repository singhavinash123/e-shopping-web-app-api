package com.eshop.web.app.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eshop.web.app.components.Translator;
import com.eshop.web.app.entity.Category;
import com.eshop.web.app.models.AllCategoriesResponse;
import com.eshop.web.app.models.CategoryModel;
import com.eshop.web.app.models.CatgeoryDeletedResponse;
import com.eshop.web.app.models.ErrorResponseModel;
import com.eshop.web.app.models.MessageModel;
import com.eshop.web.app.responces.CategoryResponse;
import com.eshop.web.app.service.CategoryService;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	CategoryService categoryService;
	
	@PostMapping()
	public ResponseEntity<?> createCategory(@RequestBody CategoryModel model) {
		Category category = categoryService.createCategory(model);
		if(category != null) {
			CategoryResponse categoryResponse = new CategoryResponse();
			categoryResponse.setMessage(Translator.toLocale("category.created.success"));
			categoryResponse.setCategory(category);
			return new ResponseEntity<>(categoryResponse , HttpStatus.CREATED);
		}else {
			// if category is already exists in the database...
			ErrorResponseModel errorResponse = new ErrorResponseModel();
			errorResponse.setError(new MessageModel(Translator.toLocale("category.exists")));
			errorResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			return new ResponseEntity<>(errorResponse , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping()
	public ResponseEntity<?> getAllCategories(){
		logger.info("inside the controller CategoryController start method getAllCategories");
		AllCategoriesResponse allCategoriesResponse = new AllCategoriesResponse();
		// calling service..
		List<Category> list = categoryService.getAllCategories();
		allCategoriesResponse.setCategories(list);
		logger.info("inside the controller CategoryController end method getAllCategories");
		return new ResponseEntity<>(allCategoriesResponse,HttpStatus.FOUND);
	}
	
	@DeleteMapping("/{categoryId}")
	public ResponseEntity<?> deleteCategoryById(@PathVariable("categoryId") final String categoryId) {
		logger.info("inside the controller CategoryController start method deleteCategoryById for categoryId {}",categoryId);
		Category category = categoryService.deleteCategoryById(categoryId);
		logger.info("category object in controller {}",category);
		if(category != null) {
			logger.info("inside if category {}",category);
			CatgeoryDeletedResponse catgeoryDeletedResponse = new CatgeoryDeletedResponse();
			catgeoryDeletedResponse.setMessage("Deleted Category!");
			catgeoryDeletedResponse.getResult().setN(0);
			catgeoryDeletedResponse.getResult().setOk(1);
			
			logger.info("catgeoryDeletedResponse {}",catgeoryDeletedResponse);

			return new ResponseEntity<>(catgeoryDeletedResponse,HttpStatus.OK);
		}else {
			ErrorResponseModel model = new ErrorResponseModel();
			model.setError(new MessageModel("Category Not Exists!!"));
			model.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			return new ResponseEntity<>(model,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
