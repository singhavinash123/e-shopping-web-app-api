package com.eshop.web.app.models;

import com.eshop.web.app.entity.Product;

import lombok.Data;

@Data
public class SaveProductResponse {
	private String message;
	private Product product = new Product();
}
