package com.eshop.web.app.models;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class AllOrdersResponseModel {
private int count;
private List<OrderResponseModel>  orders = new ArrayList<>();
}
