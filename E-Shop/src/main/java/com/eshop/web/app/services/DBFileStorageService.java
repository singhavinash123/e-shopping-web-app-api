package com.eshop.web.app.services;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.processing.FilerException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.eshop.web.app.components.Translator;
import com.eshop.web.app.entity.FileDetailsDb;
import com.eshop.web.app.exceptions.FileExceptions;
import com.eshop.web.app.repository.FileRepository;

@Service
public class DBFileStorageService {
	
	String fileName = null;
	
	@Autowired
	FileRepository fileRepository;
	
	public FileDetailsDb storeFile(MultipartFile file) throws Exception {
		checkFileValidations(file);
		try {
			FileDetailsDb fileDetailsDb = new FileDetailsDb(fileName, file.getContentType(), file.getBytes());
			return fileRepository.save(fileDetailsDb);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void checkFileValidations(MultipartFile file) {
		fileName = StringUtils.cleanPath(file.getOriginalFilename());
		String fileExtension = getFileExtension(fileName);
		if(fileExtension.contains("..")) {
			throw new FileExceptions(fileName.concat(Translator.toLocale("file.extension.invalid")));
		}else if(file.getSize() > 512000){
			throw new FileExceptions(fileName.concat("file size is not allowed!!"));
		}else if(!fileExtension.equalsIgnoreCase("JPEG") && !fileExtension.equalsIgnoreCase("PNG") && !fileExtension.equalsIgnoreCase("JPG")) {
			throw new FileExceptions(fileName.concat(" ").concat("file extension is not allowed!!"));
		}
	}

	private String getFileExtension(String fileName) {
		if(fileName.lastIndexOf('.') != -1) {
			return fileName.substring(fileName.lastIndexOf('.')+1);
		}
		return "";
	}

	public FileDetailsDb getDownloadFile(String fileId) {
		return fileRepository.findById(fileId).orElse(null);
	}
}
