package com.eshop.web.app.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.eshop.web.app.components.Translator;
import com.eshop.web.app.entity.Category;
import com.eshop.web.app.entity.FileDetailsDb;
import com.eshop.web.app.entity.Product;
import com.eshop.web.app.models.ErrorResponseModel;
import com.eshop.web.app.models.GetAllProductsResponse;
import com.eshop.web.app.models.MessageModel;
import com.eshop.web.app.models.SaveProductResponse;
import com.eshop.web.app.service.CategoryService;
import com.eshop.web.app.service.ProductService;
import com.eshop.web.app.services.DBFileStorageService;


//@Controller  (jsp,html,jquery)
@RestController
@RequestMapping("/api/products")
public class ProductController {
	private static final String STATIC_DOWNLOAD_FILE_URL = "/api/products"+"/uploads/";

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ProductService  productService;

	@Autowired
	CategoryService categoryService;

	@Autowired
	DBFileStorageService dBFileStorageService;

	@PostMapping()
	public ResponseEntity<?> createProduct(
			@RequestParam("name") String name,
			@RequestParam("price") String price,
			@RequestParam("category") String category,
			@RequestParam(value="productImage", required=false) MultipartFile file,
			HttpServletRequest request
			) {
		logger.info("{}", "Inside ProductController started method createProduct!!");
		logger.info("{} , {}", name , file);
		FileDetailsDb dbStoreFile;
		try {
			// save the file into the database...
			dbStoreFile = dBFileStorageService.storeFile(file);
			if(dbStoreFile != null) {
				//api/products/uploads/991aec53-de0f-4e35-8d65-604cc05de6e6
				String fileDownloadURL = STATIC_DOWNLOAD_FILE_URL.concat(dbStoreFile.getId());
				Product product = new Product();
				product.setName(name);
				product.setPrice(Double.parseDouble(price));
				product.setProductImage(fileDownloadURL);
				// find the category by its categoryId...
				Category cat = categoryService.findCategoryById(category);
				if(cat != null) {
					product.setCategory(cat);
					// save the file into the database...
					Product savedProduct = productService.createProduct(product);
					if(savedProduct != null) {
						SaveProductResponse resp = new SaveProductResponse();
						resp.setMessage("Product created successfully!");
						resp.setProduct(savedProduct);
						return new ResponseEntity<>(resp,HttpStatus.OK);
					}else {
						ErrorResponseModel resp = new ErrorResponseModel();
						resp.setError(new MessageModel(Translator.toLocale("product,created.false")));
						return new ResponseEntity<>(resp,HttpStatus.INTERNAL_SERVER_ERROR);
					}
				} else {
					ErrorResponseModel resp = new ErrorResponseModel();
					resp.setError(new MessageModel("Category not present, so product can't create"));
					return new ResponseEntity<>(resp,HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
		} catch (Exception e) {
			ErrorResponseModel resp = new ErrorResponseModel();
			resp.setError(new MessageModel(e.getMessage()));
			return new ResponseEntity<>(resp,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("{}", "Inside ProductController end method createProduct!!");
		return null;
	}
	
	@PutMapping("/{prouductId}")
	public ResponseEntity<?> updateProduct(
			@RequestParam("name") String name,
			@RequestParam("price") String price,
			@RequestParam("category") String category,
			@RequestParam(value="productImage", required=false) MultipartFile file,
			HttpServletRequest request
			){
		
		return null;
	}
	
	
	@GetMapping("/uploads/{fileId}")
	public ResponseEntity<?> downloadFile(@PathVariable String fileId) {
		FileDetailsDb fileDetailsDb = dBFileStorageService.getDownloadFile(fileId);
		return new ResponseEntity<>(fileDetailsDb,HttpStatus.OK);
	}
	
	@GetMapping("/{productId}")
	public ResponseEntity<?> getProductByID(@PathVariable("productId") String productId) {
		Product product = productService.getProductByID(productId);
		if(product != null) {
			return new ResponseEntity<>(product , HttpStatus.OK);
		}
		MessageModel model = new MessageModel("Product Not Found!");
		return new ResponseEntity<>(model , HttpStatus.NOT_FOUND);
	}
	
	@GetMapping()
	public ResponseEntity<?> getAllProducts(
				@RequestParam(value="category", defaultValue = "" , required=false) String categoryId
			) {
		logger.info("{}", "Inside ProductController start method getAllProducts!!");
		GetAllProductsResponse getAllProductsResponse = null;
		if(categoryId.length() > 0) {
			logger.info("categoryId.length() {}",categoryId.length());
			// do the filter for this category
			getAllProductsResponse =  productService.getAllProductsByCategory(categoryId);
			if(getAllProductsResponse == null) {
				ErrorResponseModel resp = new ErrorResponseModel();
				resp.setError(new MessageModel("Product Not Found For this Category!"));
				return new ResponseEntity<>(resp , HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<>(getAllProductsResponse , HttpStatus.OK);
		}
		// dont do any filter for anything return all the products..
		getAllProductsResponse = productService.getAllProducts();
		logger.info("getAllProductsResponse = {}", getAllProductsResponse);
		logger.info("Inside ProductController end method getAllProducts!!");
		return new ResponseEntity<>(getAllProductsResponse , HttpStatus.OK);
	}
	

}
