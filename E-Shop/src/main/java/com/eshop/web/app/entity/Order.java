package com.eshop.web.app.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@Entity
@Table(name="orders")
public class Order extends Auditable<String> implements Serializable{

	@Id
	@GeneratedValue(generator = "uuid" , strategy = GenerationType.IDENTITY)
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@JsonProperty("_id")
	private String id;

	private String firstName;
	private String lastName;
	private String address;

	private String orderStatus="pending";
	private String orderPaymentMode="COD";

	@JsonProperty("user")
	private String userId;


	@JsonProperty("product")
	private String productId;

	private int quantity;

	private double price;

}
