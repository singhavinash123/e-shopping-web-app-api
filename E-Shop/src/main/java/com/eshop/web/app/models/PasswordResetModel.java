package com.eshop.web.app.models;

import lombok.Data;

@Data
public class PasswordResetModel {
	private String token;
	private String password;
}
