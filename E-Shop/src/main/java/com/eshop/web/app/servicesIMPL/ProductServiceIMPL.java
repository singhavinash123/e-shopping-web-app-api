package com.eshop.web.app.servicesIMPL;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eshop.web.app.entity.Product;
import com.eshop.web.app.models.GetAllProductsResponse;
import com.eshop.web.app.repository.ProductRepository;
import com.eshop.web.app.service.ProductService;

@Service
public class ProductServiceIMPL implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Product createProduct(Product product) {
		return productRepository.save(product);
	}

	@Override
	public Product getProductByID(String productId) {
		return productRepository.findById(productId).orElse(null);
	}

	@Override
	public GetAllProductsResponse getAllProducts() {
		List<Product> products = productRepository.findAll();
		GetAllProductsResponse getAllProductsResponse = new GetAllProductsResponse();
		getAllProductsResponse.setCount(products.size());
		getAllProductsResponse.setProducts(products);
		return getAllProductsResponse;
	}

	@Override
	public GetAllProductsResponse getAllProductsByCategory(String categoryId) {
//		List<Product> list = productRepository.findAll();
//		List<Product> filteredProducts = new ArrayList<Product>();
//		for(Product product : list) {
//			String cateId = product.getCategory().getId();
//			if(cateId.equals(categoryId)) {
//				filteredProducts.add(product);
//			}
//		}
//		
		List<Product> products = productRepository.findAll().stream().filter(p -> p.getCategory().getId().equals(categoryId)).collect(Collectors.toList());
		if(products.size() <= 0) {
			return null;
		}
		GetAllProductsResponse getAllProductsResponse = new GetAllProductsResponse();
		getAllProductsResponse.setCount(products.size());
		getAllProductsResponse.setProducts(products);
		return getAllProductsResponse;
	}

	@Override
	public List<Product> findAll() {
		return productRepository.findAll();
	}
}
