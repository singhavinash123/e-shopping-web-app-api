package com.eshop.web.app.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CategoryModel {
	private String title;
}
