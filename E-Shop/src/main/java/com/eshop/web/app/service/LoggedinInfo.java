package com.eshop.web.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.eshop.web.app.entity.User;

@Service
public class LoggedinInfo {

	@Autowired
	private UserService userService;
	
	public User getLoggedinUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(authentication == null || !authentication.isAuthenticated()) {
			return null;
		}
		// mail id...
		String userName = ((UserDetails)authentication.getPrincipal()).getUsername();
		return userService.loadUserByUsername(userName);
	}
}
