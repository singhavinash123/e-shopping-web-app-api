package com.eshop.web.app.models;

import java.util.ArrayList;
import java.util.List;

import com.eshop.web.app.entity.Order;

import lombok.Data;

@Data
public class SavedOrderResponseModel {
	private String message;
	private List<Order> orders = new ArrayList<>();
}
