package com.eshop.web.app.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.eshop.web.app.entity.User;

import lombok.Data;

@Data
@Entity
@Table(name="password_reset_tokens")
public class PasswordResetTokenEntity {

	@Id
	@GeneratedValue(generator = "uuid" , strategy = GenerationType.IDENTITY)
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	private String token;
	
	@OneToOne()
	@JoinColumn(name="user_id")
	private User user;
	
}
