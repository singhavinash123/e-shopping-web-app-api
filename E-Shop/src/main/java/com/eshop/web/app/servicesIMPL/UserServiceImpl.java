package com.eshop.web.app.servicesIMPL;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javax.mail.internet.AddressException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.eshop.web.app.entity.User;
import com.eshop.web.app.exceptions.UserNotFoundException;
import com.eshop.web.app.models.PasswordResetTokenEntity;
import com.eshop.web.app.models.UserRequestModel;
import com.eshop.web.app.repository.PasswordResetTokenRepository;
import com.eshop.web.app.repository.UserRepository;
import com.eshop.web.app.service.UserService;
import com.eshop.web.app.util.MailsUtils;
import com.eshop.web.app.util.StringUtils;

/*
 * Service class will have business logic...
 */
@Service
public class UserServiceImpl implements UserService{

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	/*
	 * one directional encoder, when it will encrypt it can not be decrypt again
	 */
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	StringUtils stringUtils;
	
	@Autowired
	MailsUtils mailsUtils;
	
	@Autowired
	private PasswordResetTokenRepository passwordResetTokenRepository;

	/*
	 * Writing out business logic for inserting user record into the database..
	 */
	@Override
	public User createUser(UserRequestModel model) {
		User newUser = null;
		if(userRepository.findByEmail(model.getEmail()) != null) {
			return newUser;
		}else {
			newUser = new User();
			String encyptedPassword = bCryptPasswordEncoder.encode(model.getPassword());
			newUser.setEmail(model.getEmail());
			newUser.setPassword(encyptedPassword);
			newUser.setPhone(model.getPhone());
			newUser.setName(model.getName());
			newUser.setUserName(model.getEmail());
			newUser.setUserType("user");
			
			Timestamp timestamp = new Timestamp(new Date().getTime());
			newUser.setCreatedAt(timestamp);
			newUser.setUpdatedAt(timestamp);
			newUser =  userRepository.save(newUser);
		}

		return newUser;
	}

	@Override
	public User loadUserByUsername(String username) {
		return userRepository.findByEmail(username);
	}

	@Override
	public User findByUserId(String userId) {
		return userRepository.findById(userId).orElse(null);
	}

	@Override
	public String findAllUserDetailsBetweenDate(Date before30DaysDate,Date todayDate) {
		// 03 oct
		// 03 sep
		List<User> listUser = userRepository.findAll();
		return String.valueOf(listUser.stream().filter(u -> u.getCreatedAt().before(todayDate) && u.getCreatedAt().
				after(before30DaysDate)).collect(Collectors.toList()).size());
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	public Boolean requestPasswordReset(String email) {
		boolean returnflag = true;
		User user = userRepository.findByEmail(email);
		System.out.println(user);
		if(user != null) {
			// generate a reset password link....
			String token = stringUtils.generatePasswordResetToken(user.getId());
			PasswordResetTokenEntity  passwordResetTokenEntity = new PasswordResetTokenEntity();
			passwordResetTokenEntity.setToken(token);
			passwordResetTokenEntity.setUser(user);
			
			PasswordResetTokenEntity saveEntity = passwordResetTokenRepository.save(passwordResetTokenEntity);
			if(saveEntity.getId() != null) {
				ExecutorService executorService = Executors.newFixedThreadPool(5);
				
				executorService.execute(new Runnable() {
					@Override
					public void run() {
						try {
							mailsUtils.sendPasswordResetToken(user.getName(), user.getEmail(), token);
						} catch (AddressException e) {
							e.printStackTrace();
						}
					}
				});
				
			}
		}else {
			return false;
		}
		return returnflag;
	}

	public boolean resetPassword(String token, String confirmPassword) {
		boolean returnValue = false;
		if(stringUtils.isTokenExpired(token)) {
			throw new UserNotFoundException("user.password.linkNotExist");
		}
		PasswordResetTokenEntity passwordResetTokenEntity = passwordResetTokenRepository.findByToken(token);
		if(passwordResetTokenEntity == null) {
			return returnValue;
		}
		String encryptedPassword = bCryptPasswordEncoder.encode(confirmPassword);
		User user = passwordResetTokenEntity.getUser();
		
		user.setPassword(encryptedPassword);
		
		User updatedUser = userRepository.save(user);
		if(updatedUser.getPassword().equalsIgnoreCase(encryptedPassword)) {
			returnValue = true;
		}
		passwordResetTokenRepository.delete(passwordResetTokenEntity);
		return returnValue;
	}
}
