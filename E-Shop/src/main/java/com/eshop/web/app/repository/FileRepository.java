package com.eshop.web.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eshop.web.app.entity.FileDetailsDb;

@Repository
public interface FileRepository extends JpaRepository<FileDetailsDb, String>{
	
}
