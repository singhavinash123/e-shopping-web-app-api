package com.eshop.web.app.util;

import java.util.Calendar;
import java.util.Date;

public class Utils {

	private static Date date = null;
	
	private Utils() {
		
	}
	
	public static Date getTodayDate(){
		if(date != null) {
			return date;
		}
		return new Date();
	}

	public static Date substractDays(Date todayDate, int days) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -days);
		return cal.getTime();
	}
}
