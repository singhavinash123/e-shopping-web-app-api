package com.eshop.web.app.servicesIMPL;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eshop.web.app.entity.Order;
import com.eshop.web.app.entity.User;
import com.eshop.web.app.enums.PaymentMethodEnum;
import com.eshop.web.app.models.AllOrdersResponseModel;
import com.eshop.web.app.models.OrderDTO;
import com.eshop.web.app.models.OrderResponseModel;
import com.eshop.web.app.models.ProductDTO;
import com.eshop.web.app.models.SavedOrderResponseModel;
import com.eshop.web.app.repository.OrderRepository;
import com.eshop.web.app.service.LoggedinInfo;
import com.eshop.web.app.service.OrderService;
import com.eshop.web.app.service.ProductService;
import com.eshop.web.app.service.UserService;

@Service
public class OrderServiceIMPL implements OrderService{
	@Autowired
	private LoggedinInfo loggedinInfo;
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	UserService userService;
	
	@Autowired
	ProductService productService;

	@Override
	public SavedOrderResponseModel createOrder(OrderDTO orderDto) {
		SavedOrderResponseModel response = new SavedOrderResponseModel();
		for(ProductDTO productOrder : orderDto.getProducts()) {
			Order order = new Order();
			order.setAddress(orderDto.getAddress());
			order.setFirstName(orderDto.getFirstName());
			order.setLastName(orderDto.getLastName());
			order.setProductId(productOrder.getProductId());
			order.setPrice(productOrder.getPrice());
			order.setQuantity(productOrder.getQuantity());
			order.setUserId(loggedinInfo.getLoggedinUser().getId());
			orderRepository.save(order);
			response.getOrders().add(order);
		}
		response.setMessage("Orders was Created!!");
		return response;
	}

	@Override
	public AllOrdersResponseModel getAllOrders() {
		AllOrdersResponseModel model = new AllOrdersResponseModel();
		List<Order> orderList = orderRepository.findAll();
		return getResponseModel(orderList);
	}

	@Override
	public AllOrdersResponseModel getAllOrders(String userId) {
		if(userId != null && userId.length() > 0) {
			List<Order> orderList = orderRepository.findByUserId(userId);
			return getResponseModel(orderList);
		}
		List<Order> orderList = orderRepository.findAll();
		return getResponseModel(orderList);
	}
	
	AllOrdersResponseModel getResponseModel(List<Order> orderList){
		AllOrdersResponseModel model = new AllOrdersResponseModel();
		for(Order order : orderList) {
			OrderResponseModel orderResponseModel = new OrderResponseModel();
			orderResponseModel.setAddress(order.getAddress());
			orderResponseModel.setFirstName(order.getFirstName());
			orderResponseModel.setLastName(order.getLastName());
			orderResponseModel.setPaymentMethod(order.getOrderPaymentMode());
			orderResponseModel.setOrderId(order.getId());
			orderResponseModel.setStatus(order.getOrderStatus());
			orderResponseModel.setPrice(order.getPrice());
			orderResponseModel.setQuantity(order.getQuantity());
			User user = userService.findByUserId(order.getUserId());
			orderResponseModel.setUser(user);
			orderResponseModel.setProduct(productService.getProductByID(order.getProductId()));
			orderResponseModel.setCreatedAt(order.getCreatedDate());
			orderResponseModel.setUpdatedAt(order.getLastModifiedDate());
			model.getOrders().add(orderResponseModel);
		}
		model.setCount(orderList.size());
		return model;
	}

	@Override
	public Boolean updateOrderStatus(String orderId) {
		Optional<Order> order = orderRepository.findById(orderId);
		if(order.isPresent()) {
			Order savedOrder = order.get();
			savedOrder.setOrderStatus(PaymentMethodEnum.COMPLETED.status());
			orderRepository.save(savedOrder);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	@Override
	public String findAllOrdersBetweenDate(Date before30DaysDate, Date todayDate) {
		// findAll Orders
		// filter the list of orders and then  get the orders bewtween given date 
		// it will take 5 to 6 minuts  time
		return String.valueOf(orderRepository.findAllOrdersBetweenDate(before30DaysDate,todayDate).size());
	}

	@Override
	public String findTotalSalesBetweenDate(Date before30DaysDate, Date todayDate) {
		return String.valueOf(orderRepository.findTotalSalesBetweenDate(before30DaysDate,todayDate));
	}

	@Override
	public List<Order> findAllOrdersDetailsBetweenDate(Date before30DaysDate, Date todayDate) {
		return orderRepository.findAllOrdersBetweenDate(before30DaysDate,todayDate);
	}

	@Override
	public List<Order> findAll() {
		return orderRepository.findAll();
	}

}
