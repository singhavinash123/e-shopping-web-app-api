package com.eshop.web.app.util;


import java.util.Date;

import org.springframework.stereotype.Component;

import com.eshop.web.app.constants.SecurityConstants;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class StringUtils {

	public String removeLastOccuranceString(String str){
		if (null != str && str.length() > 0 ){
			return str.substring(0, str.lastIndexOf('/'));
		}
		return str;
	}

	public String generatePasswordResetToken(String id) {
		return Jwts.builder()
		.setSubject(id)
		.setExpiration(new Date(System.currentTimeMillis()+SecurityConstants.PASSWORD_RESTE_EXPIRATION_TIME))
		.signWith(SignatureAlgorithm.HS512, SecurityConstants.getTokenSecret())
		.compact();
	}

	public boolean isTokenExpired(String token) {
		Claims claims = Jwts.parser()
				.setSigningKey(SecurityConstants.getTokenSecret())
				.parseClaimsJws(token).getBody();
		Date tokenExpirationDate = claims.getExpiration();
		Date todayDate = new Date();
		return tokenExpirationDate.before(todayDate);
	}
}