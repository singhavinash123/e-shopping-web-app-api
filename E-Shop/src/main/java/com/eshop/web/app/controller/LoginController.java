package com.eshop.web.app.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eshop.web.app.components.Translator;
import com.eshop.web.app.entity.User;
import com.eshop.web.app.models.AuthRequest;
import com.eshop.web.app.models.ErrorResponseModel;
import com.eshop.web.app.models.MessageModel;
import com.eshop.web.app.models.PasswordResetModel;
import com.eshop.web.app.models.PasswordResetRequestModel;
import com.eshop.web.app.models.ResetLinkSentResponseModel;
import com.eshop.web.app.models.TokenResponseModel;
import com.eshop.web.app.models.UserRequestModel;
import com.eshop.web.app.service.LoggedinInfo;
import com.eshop.web.app.servicesIMPL.UserServiceImpl;
import com.eshop.web.app.util.JwtUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;

@ApiOperation(value = "", authorizations = { @Authorization(value="jwtToken") })
@RestController
@RequestMapping("/api/users")
public class LoginController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@GetMapping("/test")
	public String testInternationalization() {
		return Translator.toLocale("hello");
	}

	@Autowired
	UserServiceImpl userServiceImpl;

	@Autowired
	private LoggedinInfo loggedinInfo;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private AuthenticationManager authenticationManager;

	@PostMapping("/login")
	public ResponseEntity<?> generateToken(@RequestBody @Valid AuthRequest authRequest,BindingResult result){
		logger.info("INSIDE THE WelcomeController AND generateToken  METHOD!!");
		if (result.hasErrors()) {
			System.out.println(result.getAllErrors());
		}
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							authRequest.getEmail(),
							authRequest.getPassword())
					);
		} catch (Exception ex) {
			ErrorResponseModel errorResponse = new ErrorResponseModel();
			errorResponse.setError(new MessageModel(Translator.toLocale("user.auth.failed")));
			errorResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		TokenResponseModel tokenModel = new TokenResponseModel(Translator.toLocale("user.auth.success"),
				jwtUtil.generateToken(authRequest.getEmail())
				);
		return new ResponseEntity<>(tokenModel, HttpStatus.OK);
	}

	@PostMapping("/signup")
	public ResponseEntity<?> createUser(@RequestBody UserRequestModel requestModel) {
		logger.info("{}",requestModel);
		User user = userServiceImpl.createUser(requestModel);
		if(user != null) {
			// user created!!
			MessageModel model = new MessageModel(Translator.toLocale("user.created.success"));
			return new ResponseEntity<>(model, HttpStatus.CREATED);
		}else {
			// user exists!!!
			ErrorResponseModel errorResponse = new ErrorResponseModel();
			errorResponse.setError(new MessageModel(Translator.toLocale("user.already.exists")));
			return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("/is-admin")
	public ResponseEntity<?> checkAdmin(){
		User user = loggedinInfo.getLoggedinUser();
		if(user == null) {
			ErrorResponseModel errorModel = new ErrorResponseModel();
			errorModel.setError(new MessageModel(Translator.toLocale("user.auth.failed")));
			return new ResponseEntity<>(errorModel , HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(user.getUserType().equalsIgnoreCase("admin") , HttpStatus.OK);
	}


	@PostMapping("/reset-password")
	public ResponseEntity<?> passwordResetRequest(@RequestBody PasswordResetRequestModel passwordResetRequestModel) {
		logger.info("Inside LoginController start method passwordResetRequest!!");
		Boolean operationResult = userServiceImpl.requestPasswordReset(passwordResetRequestModel.getEmail());
		if(operationResult) {
			ResetLinkSentResponseModel response = new ResetLinkSentResponseModel();
			response.setMessage("Link has been sent to you email id!!");
			return new ResponseEntity<>(response,HttpStatus.OK);
		}else {
			System.out.println("when email not exists!!");
			ErrorResponseModel errorModel = new ErrorResponseModel();
			errorModel.setError(new MessageModel("Something went wrong"));
			errorModel.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			return new ResponseEntity<>(errorModel,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	// send a token with new password...
	// check the token is valid and active and then if active then reset the password in the data base...
	@PostMapping("/confirm-password-reset")
	public ResponseEntity<?> resetPassword(@RequestBody PasswordResetModel passwordResetModel){
		logger.info("Inside LoginController start method resetPassword!!");
		boolean operationResult = userServiceImpl.resetPassword(passwordResetModel.getToken(),passwordResetModel.getPassword());
		ResetLinkSentResponseModel response = new ResetLinkSentResponseModel();
		if(operationResult) {
			response.setMessage("user.auth.passwordReset");
		}
		return new ResponseEntity<>(response,HttpStatus.OK);
	}
}
