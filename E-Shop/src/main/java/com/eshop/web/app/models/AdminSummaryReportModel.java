package com.eshop.web.app.models;

import lombok.Data;

@Data
public class AdminSummaryReportModel {
	private Last30DaysSummary last30DaysSummary = new Last30DaysSummary();
	private OverAll overAll = new OverAll();
}
