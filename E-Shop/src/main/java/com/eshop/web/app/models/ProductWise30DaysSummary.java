package com.eshop.web.app.models;

import com.eshop.web.app.entity.Product;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ProductWise30DaysSummary {
	@JsonProperty("_id")
	private String productId;
	private int quantity;
	private double totalSale;
	private Product product;
}
