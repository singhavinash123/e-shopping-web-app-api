package com.eshop.web.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eshop.web.app.models.ErrorResponseModel;
import com.eshop.web.app.models.MessageModel;
import com.eshop.web.app.models.OrderDTO;
import com.eshop.web.app.models.OrderStatusRequest;
import com.eshop.web.app.models.OrderUpdateResponseModel;
import com.eshop.web.app.models.SavedOrderResponseModel;
import com.eshop.web.app.service.LoggedinInfo;
import com.eshop.web.app.service.OrderService;

@RestController
//@Controller
@RequestMapping("/api/orders")
public class OrderController {

	@Autowired
	OrderService orderService;

	@Autowired
	private LoggedinInfo loggedinInfo;

	//	@PostMapping("/")
	@RequestMapping(value="/" , method=RequestMethod.POST)
	public ResponseEntity<?> createOrder(@RequestBody OrderDTO orderDto) {
		SavedOrderResponseModel  savedOrder = orderService.createOrder(orderDto);
		return new ResponseEntity<>(savedOrder, HttpStatus.OK);
	}

	//	@GetMapping()
	//	public ResponseEntity<?> getAllOrders() {
	//		AllOrdersResponseModel responseModel = orderService.getAllOrders();
	//		return new ResponseEntity<>(responseModel,HttpStatus.OK);
	//	}

	@GetMapping()
	public ResponseEntity<?> getAllOrders(@RequestParam(value="all", defaultValue = "" , required = false) String a){
		if(a.length() > 0) {
			// return all the orders..
			return new ResponseEntity<>(orderService.getAllOrders(null),HttpStatus.OK);
		} 
		// return all the orders for logged in user
		return new ResponseEntity<>(orderService.getAllOrders(loggedinInfo.getLoggedinUser().getId()), HttpStatus.OK);
	}

	@PatchMapping("/{orderId}")
	public ResponseEntity<?> updateOrderStatus(@PathVariable("orderId") String orderId,
			@RequestBody OrderStatusRequest request){
		if(loggedinInfo.getLoggedinUser().getUserType().equalsIgnoreCase("admin")) {
			Boolean status = orderService.updateOrderStatus(orderId);
			if(status == true) {
				OrderUpdateResponseModel model = new OrderUpdateResponseModel(); 
				model.setStatus(request.getStatus());
				return new ResponseEntity<>(model,HttpStatus.OK);
			}
		}

		ErrorResponseModel error = new ErrorResponseModel();
		error.setError(new MessageModel("something went wrong!!"));
		error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		return new ResponseEntity<>(error,HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
