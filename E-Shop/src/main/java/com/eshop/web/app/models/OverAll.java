package com.eshop.web.app.models;

import lombok.Data;

@Data
public class OverAll {
	private int products;
	private int orders;
	private int users;
}
