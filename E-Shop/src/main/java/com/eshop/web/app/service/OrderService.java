package com.eshop.web.app.service;

import java.util.Date;
import java.util.List;

import com.eshop.web.app.entity.Order;
import com.eshop.web.app.models.AllOrdersResponseModel;
import com.eshop.web.app.models.OrderDTO;
import com.eshop.web.app.models.SavedOrderResponseModel;

public interface OrderService {
	SavedOrderResponseModel createOrder(OrderDTO orderDto);
	AllOrdersResponseModel getAllOrders();
	AllOrdersResponseModel getAllOrders(String userId);
	Boolean updateOrderStatus(String orderId);
	String findAllOrdersBetweenDate(Date before30DaysDate, Date todayDate);
	String findTotalSalesBetweenDate(Date before30DaysDate, Date todayDate);
	List<Order> findAllOrdersDetailsBetweenDate(Date before30DaysDate, Date todayDate);
	List<Order> findAll();
}
