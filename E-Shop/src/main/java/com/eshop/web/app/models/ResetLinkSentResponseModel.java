package com.eshop.web.app.models;

import lombok.Data;

@Data
public class ResetLinkSentResponseModel {
		private String message;
}
