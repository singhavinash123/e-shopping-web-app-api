package com.eshop.web.app.models;

import lombok.Data;

@Data
public class Result {
	private int n;
	private int ok;
}
