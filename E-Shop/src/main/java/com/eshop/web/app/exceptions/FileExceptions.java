package com.eshop.web.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class FileExceptions extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4996522804420953614L;

	public FileExceptions(String message){
		super(message);
	}
}
