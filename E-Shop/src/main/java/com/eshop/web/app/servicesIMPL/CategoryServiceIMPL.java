package com.eshop.web.app.servicesIMPL;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eshop.web.app.entity.Category;
import com.eshop.web.app.models.CategoryModel;
import com.eshop.web.app.repository.CategoryRepository;
import com.eshop.web.app.service.CategoryService;

@Service
public class CategoryServiceIMPL implements CategoryService{
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	CategoryRepository categoryRepository;

	@Override
	public Category createCategory(CategoryModel model) {
		Category category = categoryRepository.findByName(model.getTitle());
		if(category != null) {
			return null;
		}
		category = new Category();
		category.setName(model.getTitle());
		
		return categoryRepository.save(category);
	}

	@Override
	public Category findCategoryById(String categoryId) {
		return categoryRepository.findById(categoryId).orElse(null);
	}

	@Override
	public List<Category> getAllCategories() {
		return categoryRepository.findAll();
	}

	@Override
	public Category deleteCategoryById(String categoryId) {
		logger.info("categoryId ==> {}",categoryId);
		Category category = categoryRepository.findById(categoryId).get();
		if(category != null) {
			logger.info("category ==> {}",category);

			categoryRepository.delete(category);
			return category;
		}
		
		logger.info("if category null ==> {}",category);
		return null;
	}
}
